
def samples_paths(n,low,high):

    for n_samples in range(low,high+1,100):
            n_gram_path = f'results/1_grams_{n_samples}-{n_samples+100}.csv'
            yield [n_gram_path]

    for i in range(1+1,n+1):
        for n_samples in range(low,high+1,100):
            n_gram_path = f'results/{i}_grams_{n_samples}-{n_samples+100}.csv'
            sn_gram_path = f'results/s{i}_grams_{n_samples}-{n_samples+100}.csv'
            yield [n_gram_path,sn_gram_path]

for i in samples_paths(3,100,300):
    for j in i:
        print(j)