import sys
import core.samples_extraction as se
import core.create as create
import core.nlp as nlp
import core.datascience as dt


def create_dict(d):
    new_d = dict()

    for k,v in d.items():
        new_d.update({k : []})
    return new_d

def produce():
    authors = {}
    authors["Emmanuel"] = [
        "Há dois mil anos",
        "Cinquenta Anos Depois",
    ]
    authors['André Luiz'] = [
        "Ação e Reação",
        "Libertação",
        "Os Mensageiros"
    ]
    authors['Humberto'] = [
        "Brasil Coracao do Mundo Patria do Evangelho", 
        "Cronicas de Alem Turmulo", 
        "Reportagens de alem tumulo"
        ]

    authors['Humberto de Campos'] = [
        "A serpente de bronze",
        "Grãos de mostarda",
        "O Monstro e Outros"
    ]

    authors["Irmão X"] = [
        "Cartas e Crônicas",
        "Contos desta e doutra vida",
        "Lázaro Redivivo",
        "Luz Acima",
        "Pontos e Contos",
        "Relatos da Vida" 
    ]

    for author,books in authors.items():
        for book in books:
            data = se.get_text(author, book)

            for n_samples in range(100,301,100):
                low,high = n_samples, n_samples+100    
                texts = se.build_texts(data, low, high)
                se.write_file((author,book),texts,f'{low}-{high}')

    spacy_nlp = nlp.SpacySingleton.instance()

    for n_samples in range(100,301,100):
        filename = f'{n_samples}-{n_samples+100}.json'
        for n in range(1,4):
            texts = create_dict(authors)
            for author,books in authors.items():
                for book in books:
                    texts[f'{author}'].append(create.read_to_list(author, book, filename))
            data_n = nlp.n_gram_to_dict(spacy_nlp, texts, n)
            create.save_to_csv(f"{n}_grams_{n_samples}-{n_samples+100}.csv",data_n)

            data_sn = nlp.sn_gram_to_dict(spacy_nlp,texts, n)
            create.save_to_csv(f"s{n}_grams_{n_samples}-{n_samples+100}.csv",data_sn)

def machine(estimator):
    if(not estimator):
        print("Choose svm, mlp or rf")
    else:
        dt.run(estimator)


if __name__ == "__main__":
    if(sys.argv[1] == "--produce"):
        produce()
    elif(sys.argv[1] == "--machine"):
        machine(sys.argv[2])
    else:
        pass