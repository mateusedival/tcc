import pandas as pd
import json
from pathlib import Path

def save_to_csv(filename,data):
    
    filepath = f'{Path.cwd()}\\results\\{filename}'
    df = pd.DataFrame(data=data)
    df.to_csv(filepath, index=False)

def read_to_list(author,book,filename):

    texts = []
    filepath = f'{Path.cwd()}\\samples\\{author}\\{book}\\{filename}'
    with open(filepath,"r") as file:
            texts = json.load(file)
    return texts


def write_json(name, data):
    jsonstr = json.dumps(data)
    filepath = f'{Path.cwd()}/data/'
    with open(f'{filepath}/{name}.json',"w") as file:
        file.write(jsonstr)

def javascript_object(path,
    results_accuracy,
    results_weighted_f1_score,
    results_weighted_precision,
    results_weighted_recall,
    results_f1_score,
    results_precision,
    results_recall,
    results_bests_params,
    accuracy_mean,
    accuracy_var,
    f1_score_mean,
    f1_score_var,
    precision_mean,
    precision_var,
    recall_mean,
    recall_var):

    obj = dict({"path": path,
                "results_accuracy": results_accuracy,
                "results_weighted_f1_score": results_weighted_f1_score,
                "results_weighted_precision": results_weighted_precision,
                "results_weighted_recall": results_weighted_recall,
                "results_f1_score": results_f1_score,
                "results_precision": results_precision,
                "results_recall": results_recall,
                "results_bests_params": results_bests_params,
                "accuracy_mean": accuracy_mean,
                "accuracy_var": accuracy_var,
                "f1_score_mean": f1_score_mean,
                "f1_score_var": f1_score_var,
                "precision_mean": precision_mean,
                "precision_var": precision_var,
                "recall_mean": recall_mean,
                "recall_var": recall_var})

    return obj
    
    