import re
import sys
from pathlib import Path
import json

def get_text(author,book):
    """"Retorna o texto de um arquivo txt"""
    
    filepath = f'{Path.cwd()}\\books\{author}\{book}.txt'
    with open(filepath, encoding= "utf8") as t:
        data = t.read()
    return data

def range_words(words, low, high):
    """Checa se a quantidade de palavras de uma lista está entre [low:high["""

    if(len(words) < low):
        return -1
    elif(len(words) > high):
        return 1
    return 0


def sub_special_char(text, char, sub):
    """Retorna string com todas as ocorrências de char substituidas por sub"""

    text = re.sub(char, sub, text)
    return text

def build_texts(words, low, high):
    """Constroi cada uma das amostras a partir de uma lista de palavras"""

    texts = []
    indices = [m.start() for m in re.finditer('@',words)]
    indices.append(len(words))
    pairs = [i for i in zip(indices, indices[1:])]
    
    for a,b in pairs:
        start = a
        ends = [m.end()-1 for m in re.finditer('[\w-]+[.!?]\n',words[a:b],re.UNICODE)]
        ends.append(b-a)
        for end in ends:
            result = range_words(words[start:a+end].split(),low,high)
            if result == 0:
                texts.append(words[start:a+end])
                start = a + end + 1
            elif result > 0:
                start = a + end+1

    for idx,text in enumerate(texts):
        texts[idx] = sub_special_char(text,'@','')
        texts[idx] = sub_special_char(text,'\n',' ')
    
    return texts

def write_file(header,datas,name="texts"):
    
    author,book = header
    filepath = f'{Path.cwd()}\\samples\\{author}\\{book}\\'

    Path(filepath).mkdir(parents=True, exist_ok=True) 
    jsonstr = json.dumps(datas)
    
    with open(f'{filepath}\\{name}.json',"w") as file:
        file.write(jsonstr)