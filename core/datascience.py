import pandas as pd
import numpy as np
from sklearn.metrics import f1_score,accuracy_score,precision_score,recall_score
from sklearn.model_selection import train_test_split 
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
import core.models as models
from statistics import mean, variance 
import core.create as create

def samples_paths(n,low,high):

    for n_samples in range(low,high+1,100):
            n_gram_path = f'results/1_grams_{n_samples}-{n_samples+100}.csv'
            yield [n_gram_path]

    for i in range(1+1,n+1):
        for n_samples in range(low,high+1,100):
            n_gram_path = f'results/{i}_grams_{n_samples}-{n_samples+100}.csv'
            sn_gram_path = f'results/s{i}_grams_{n_samples}-{n_samples+100}.csv'
            yield [n_gram_path,sn_gram_path]

def run(estimator):
    json_object = []
    for paths in samples_paths(3,100,300):
        for path in paths:
            
            print(20*"=")
            print(path)
            
            df = pd.read_csv(path)

            # Normalize row by sum
            normalized_df = df[[i for i in list(df.columns) if i != 'author']].div(df.sum(axis=1), axis=0)

            df = pd.concat([normalized_df, df["author"]], axis=1)          

            results_accuracy = []
            results_weighted_f1_score = []
            results_weighted_precision = []
            results_weighted_recall = []
            results_f1_score = []
            results_precision = []
            results_recall = []
            results_bests_params = []

            for _ in range(0,10): 
                df = shuffle(df)

                X, Y = np.split(df, [len(df.columns)-1], axis=1)

                # Transform author names to a index    
                Y = LabelEncoder().fit_transform(np.ravel(Y))



                X_train, X_test, y_train, y_test = train_test_split( 
                                        X,Y,test_size = 0.30, stratify=Y) 

                y_train, y_test = np.ravel(y_train), np.ravel(y_test)

                grid = models.get_model(estimator)

                grid.fit(X_train, y_train) 
                
                grid_predictions = grid.predict(X_test) 

                results_accuracy.append(accuracy_score(y_test,grid_predictions))
                results_weighted_f1_score.append(f1_score(y_test,grid_predictions, average='weighted'))
                results_weighted_precision.append(precision_score(y_test,grid_predictions, average='weighted'))
                results_weighted_recall.append(recall_score(y_test,grid_predictions, average='weighted'))
                results_f1_score.append(f1_score(y_test,grid_predictions, average=None).tolist())
                results_precision.append(precision_score(y_test,grid_predictions, average=None).tolist())
                results_recall.append(recall_score(y_test,grid_predictions, average=None).tolist())
                results_bests_params.append(grid.best_params_)
            
            accuracy_mean = mean(results_accuracy)
            accuracy_var = variance(results_accuracy)
            f1_score_mean = mean(results_weighted_f1_score)
            f1_score_var = variance(results_weighted_f1_score)
            precision_mean = mean(results_weighted_precision)
            precision_var = variance(results_weighted_precision)
            recall_mean = mean(results_weighted_recall)
            recall_var = variance(results_weighted_recall)

            json_object.append(create.javascript_object(path,
                                                        results_accuracy,
                                                        results_weighted_f1_score,
                                                        results_weighted_precision,
                                                        results_weighted_recall,
                                                        results_f1_score,
                                                        results_precision,
                                                        results_recall,
                                                        results_bests_params,
                                                        accuracy_mean,
                                                        accuracy_var,
                                                        f1_score_mean,
                                                        f1_score_var,
                                                        precision_mean,
                                                        precision_var,
                                                        recall_mean,
                                                        recall_var))
                                                        
            create.write_json(f"resultados-{estimator}", json_object)
            print("JSON overwritten")
    
        
                