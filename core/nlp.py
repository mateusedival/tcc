import spacy


pos = ["ADJ", "ADP", "ADV", "AUX", "CONJ", "CCONJ", "DET", "INTJ", "NOUN", "NUM", "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X", "SPACE"]


class SpacySingleton:
    nlp = None

    def __init__(self):
        self.nlp = None
    
    @classmethod
    def instance(cls):
        if cls.nlp is None:
            cls.nlp = spacy.load("pt_core_news_lg")
        return cls.nlp



def n_gram(doc, n):
    l = [token.pos_ for token in doc]

    while(len(l)):
        if(len(l) >= n):
            yield '-'.join(l[0:n])
        l.pop(0)

def sn_gram(doc, idx, n, l):
    children = doc[idx].children

    l.append(doc[idx].pos_)

    for child in children:
        yield from sn_gram(doc,child.i,n,l)

    if len(l) >= n:
        yield '-'.join(l[len(l)-n:])

    l.pop(len(l)-1)

    return

def init_dict1():
    length = len(pos)

    d = dict()
    for i in range(0,length):
        key = f'{pos[i]}'
        d[key] = []
    d.update({"author" : []})
    
    return d

def init_dict2():
    length = len(pos)

    d2 = dict()
    for i in range(0,length):
        for j in range(0,length):
                key = f'{pos[i]}-{pos[j]}'
                d2[key] = []
    d2.update({"author" : []})
    
    return d2

def init_dict3():
    length = len(pos)

    d3 = dict()
    for i in range(0,length):
        for j in range(0,length):
            for t in range(0,length):
                key = f'{pos[i]}-{pos[j]}-{pos[t]}'
                d3[key] = []
    d3.update({"author" : []})

    return d3


def n_gram_to_dict(nlp, texts, n):

    d = dict()

    if(n == 1): d = init_dict1()
    elif(n == 2): d = init_dict2()
    else: d = init_dict3()


    row = 0
    for author,sample_list  in texts.items():
        for samples in sample_list:
            for doc in nlp.pipe(samples):
                for k,v in d.items():
                    if(k == "author"): continue
                    d[k].append(0)

                for att in n_gram(doc,n):
                    d[att][row] += 1
                d["author"].append(author)
                row+=1
    return d

def sn_gram_to_dict(nlp, texts, n):

    d = dict()

    if(n == 1): d = init_dict1()
    elif(n == 2): d = init_dict2()
    else: d = init_dict3()


    row = 0
    for author,sample_list  in texts.items():
        for samples in sample_list:
            for doc in nlp.pipe(samples):
                for k,v in d.items():
                    if(k == "author"): continue
                    d[k].append(0)
                for token in doc:
                    if(token.dep_ == "ROOT"):
                        l = []
                        for att in sn_gram(doc, token.i,n,l):
                            d[att][row] += 1
                d["author"].append(author)
                row+=1
    return d                  
    