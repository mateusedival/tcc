from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

def get_params(estimator_name):
    params = dict()
    if estimator_name == "svm":
        params = {'C': [0.1, 1, 10, 100],  
                'gamma': [1, 0.1, 0.01, 0.001, 0.0001], 
                'gamma':['scale', 'auto'],
                'kernel': ['linear','rbf', 'sigmoid']}

    if estimator_name == "rf":
        params = {'n_estimators': [500],
                'max_features': ['auto', 'sqrt', 'log2'],
                'max_depth': [4,6,8],
                'criterion' :['gini', 'entropy'],
                'bootstrap': [True, False]}

    if estimator_name == "mlp":
        params= {'learning_rate': ["constant", "adaptive"],
                    'activation': ['logistic','tanh', 'relu'],
                    'hidden_layer_sizes': [(100,), (100, 100,), (250, 100,),
                                            (250,), (250, 250,), (100,250,)],
                    'alpha': [0.0001, 0.05, 0.1],
                    'max_iter': [200, 500, 1000]}
    
    return params

def get_model(estimator_name):
    if estimator_name == "svm":
        estimator = svm.SVC()
        return GridSearchCV(estimator, get_params(estimator_name), verbose = 3,n_jobs=-1) 
        
    elif estimator_name == "rf":
        estimator = RandomForestClassifier()
        return GridSearchCV(estimator, get_params(estimator_name), verbose = 3,n_jobs=-1) 
    else: 
        estimator = MLPClassifier()
        return GridSearchCV(estimator, get_params(estimator_name), verbose = 3,n_jobs=-1) 
    